import os
import random

key_name1="partsizes"
key_name2="command"
key_name3="seed"
key_name4="population_size"

n = 10

for l_iter in range(10, 16):
    
    with open('deep_auto.ini', 'r') as ini_file:
        data = ini_file.readlines()

    for str_numb, line in enumerate(data):
        if key_name1 in line:
            line_list = line.split(" ")
            line_list[2] = str(l_iter * n) + ";"
            new_line = ' '.join(line_list)
            data[str_numb] = new_line
        if key_name2 in line:
            new_line = "command = nlreg -n " + str(n) + " -l " + str(l_iter) + " compl_300x5_-1-1.h5\n"
            data[str_numb] = new_line
        if key_name4 in line:
            new_line = "population_size = " + str(10*n*l_iter) + "\n"
            data[str_numb] = new_line
            
    with open('deep_auto.ini', 'w') as file:
        file.writelines(data)
        
    for rand_iter in range(1, 6):
        with open('deep_auto.ini', 'r') as ini_file:
            data = ini_file.readlines()

        for str_numb, line in enumerate(data):
            if key_name3 in line:
                line_list = line.split(" ")
                line_list[2] = str(random.randint(1, 999999)) + "\n"
                new_line = ' '.join(line_list)
                data[str_numb] = new_line
                
        with open('deep_auto.ini', 'w') as file:
            file.writelines(data)
        
        dict_name = "results/compl/l" + str(l_iter) + "/"
        time_file_name = dict_name + "compl_n" + str(n) + "_l" + str(l_iter) + "_" + str(rand_iter) + "_timeNEW.txt"
        result_file_name = dict_name + "compl_n" + str(n) + "_l" + str(l_iter) + "_" + str(rand_iter) + "NEW.txt"
        
        cmd = "/usr/bin/time -o " + time_file_name + " deepmethod --default-name=deep_auto.ini;nlreg -n " + str(n) + " -l " + str(l_iter) + " compl_300x5_-1-1.h5 deep_auto.ini-deep-output -p1 >> " + result_file_name + ";"
        os.system(cmd)
        
        os.rename("scores.txt", "results/compl/l" + str(l_iter) + "/scores" + str(rand_iter) + "_popNEW.txt")
        
        path1 = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'deep_auto.ini.log')
        path2 = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'deep_auto.ini-deep-output')
        
        os.remove(path1)
        os.remove(path2)
